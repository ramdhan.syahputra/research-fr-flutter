part of 'liveness_user_cubit.dart';

@immutable
sealed class LivenessUserState {}

final class LivenessUserInitial extends LivenessUserState {}
